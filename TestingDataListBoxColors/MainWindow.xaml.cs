﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;

namespace TestingDataListBoxColors
{
    public partial class MainWindow
    {

        public MainWindow()
        {
            InitializeComponent();
        }
    }

    public class MyData
    {
        public ICommand AddButtonClick { get; private set; }
        public ICommand DeleteButtonClick { get; private set; }

        public ObservableCollection<ListBoxDataType> SomeData { get; set; }

        public MyData()
        {
            SomeData = new ObservableCollection<ListBoxDataType>
                             {
                                 new ListBoxDataType {Name = "Test 1", Value = new CustomDataType{Valid = true}},
                                 new ListBoxDataType {Name = "Test 2", Value = new CustomDataType{Valid = false}}
                             };


            AddButtonClick = new ActionCommand(AddElement);
            DeleteButtonClick = new ActionCommand(DeleteElement);
        }

        private void AddElement()
        {
            SomeData.Add(new ListBoxDataType { Name = "Test 2", Value = new CustomDataType { Valid = false } });
            OnPropertyChange("SomeData");
        }

        private void DeleteElement()
        {
            SomeData.RemoveAt(0);
            OnPropertyChange("SomeData");
        }

        public class ListBoxDataType
        {
            public string Name { get; set; }
            public CustomDataType Value { get; set; }
        }

        public class CustomDataType
        {
            public bool Valid { get; set; }
        }



        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChange(string propertyName)
        {
            if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

    }







    public class ActionCommand : ICommand
    {
        private readonly Action _action;
        private bool _canExecute;

        public ActionCommand(Action action) : this(action, true) { }

        public ActionCommand(Action action, bool canExecute)
        {
            _action = action;
            _canExecute = canExecute;
        }

        public void Execute(object parameter)
        {
            _action();
        }

        public bool CanExecute(object parameter)
        {
            return _canExecute;
        }

        public void SetCanExecute(bool canExecute)
        {
            _canExecute = canExecute;

            if (CanExecuteChanged != null)
                CanExecuteChanged(this, EventArgs.Empty);
        }

        public event EventHandler CanExecuteChanged;
    }

}
